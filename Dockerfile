FROM node:lts-alpine as client-builder

# install simple http server for serving static content
RUN npm install -g http-server

# make the 'app' folder the current working directory
WORKDIR /app

# copy both 'package.json' and 'package-lock.json' (if available)
COPY omgevingswet-vng-client/package*.json ./

# install project dependencies
RUN npm install

# copy project files and folders to the current working directory (i.e. 'app' folder)
COPY omgevingswet-vng-client/. .

# build app for production with minification
RUN npm run build

# Assemble final image
FROM alpine

# Copy static files to target
COPY --from=client-builder /app/dist /web

RUN apk add tree
RUN tree /web
