package nl.vng.omgevingswet.service.services;

import com.fasterxml.jackson.databind.ObjectMapper;
import nl.vng.omgevingswet.service.config.OmgevingswetConfig;
import org.apache.pdfbox.pdmodel.PDDocument;
import org.apache.pdfbox.pdmodel.PDDocumentInformation;
import org.springframework.stereotype.Service;

import java.io.IOException;

@Service
public class ImportService {

    private final ObjectMapper objectMapper = new ObjectMapper();
    private final OmgevingswetConfig config;

    public ImportService(final OmgevingswetConfig config) {
        this.config = config;
    }

    public String getExportData(final byte[] input) throws IOException {
        try (final PDDocument document = PDDocument.load(input)) {
            final PDDocumentInformation info = document.getDocumentInformation();
            return info.getCustomMetadataValue(config.getMetaDataKey());
        }
    }

}
