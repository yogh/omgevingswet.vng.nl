package nl.vng.omgevingswet.service.domain;

public enum ExportStatus {

    WAITING,
    DONE

}
