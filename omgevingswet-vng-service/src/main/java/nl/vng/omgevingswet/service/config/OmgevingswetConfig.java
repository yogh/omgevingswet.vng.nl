package nl.vng.omgevingswet.service.config;

import org.springframework.context.annotation.Configuration;

@Configuration
public class OmgevingswetConfig {

    private final String metaDataKey = "VNG_ANSWERS";

    private final String chromeHeadlessExecutableName = "chromium-browser";

    private final String renderingUrl = "http://localhost:8080/export/metadata?exportKey=[KEY]";

    public String getMetaDataKey() {
        return metaDataKey;
    }

    public String getChromeHeadlessExecutableName() {
        return chromeHeadlessExecutableName;
    }

    public String getRenderingUrl() { return renderingUrl; }
}
