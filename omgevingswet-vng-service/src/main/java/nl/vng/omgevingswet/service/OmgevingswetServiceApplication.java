package nl.vng.omgevingswet.service;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class OmgevingswetServiceApplication {

    public static void main(String[] args) {
        SpringApplication.run(OmgevingswetServiceApplication.class, args);
    }

}
