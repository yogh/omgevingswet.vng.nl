package nl.vng.omgevingswet.service.controller;

import nl.vng.omgevingswet.service.services.ExportService;
import org.springframework.core.io.ByteArrayResource;
import org.springframework.core.io.Resource;
import org.springframework.http.HttpHeaders;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RestController;

@RestController
public class ExportController {

    private final ExportService exportService;

    public ExportController(final ExportService exportService) {
        this.exportService = exportService;
    }

    @PostMapping(value = "/export/start")
    public String start(@RequestBody final String exportData) {
        return exportService.startExport(exportData);
    }

    @GetMapping("/export/status")
    public String status(final String exportKey) {
        return exportService.getStatus(exportKey).name();
    }

    @GetMapping("/export/metadata")
    public String metadata(final String exportKey) {
        return exportService.getMetadata(exportKey);
    }

    @GetMapping("/export/retrieve")
    public ResponseEntity<Resource> retrieve(final String exportKey) {
        final byte[] data = exportService.getResult(exportKey);

        if (data == null) {
            return ResponseEntity.notFound().build();
        } else {
            final HttpHeaders header = new HttpHeaders();
            header.add(HttpHeaders.CONTENT_DISPOSITION, "attachment; filename=omgevingswet-status.pdf");
            header.add("Cache-Control", "no-cache, no-store, must-revalidate");
            header.add("Pragma", "no-cache");
            header.add("Expires", "0");

            final ByteArrayResource resource = new ByteArrayResource(data);

            return ResponseEntity.ok()
                    .headers(header)
                    .contentLength(resource.contentLength())
                    .contentType(MediaType.parseMediaType("application/octet-stream"))
                    .body(resource);
        }
    }

}
