package nl.vng.omgevingswet.service.services;

import com.fasterxml.jackson.databind.ObjectMapper;
import nl.vng.omgevingswet.service.config.OmgevingswetConfig;
import nl.vng.omgevingswet.service.domain.ExportStatus;
import org.apache.pdfbox.pdmodel.PDDocument;
import org.apache.pdfbox.pdmodel.PDDocumentInformation;
import org.springframework.stereotype.Service;

import java.io.ByteArrayOutputStream;
import java.io.File;
import java.io.IOException;
import java.nio.file.Files;
import java.util.List;
import java.util.UUID;
import java.util.concurrent.ConcurrentHashMap;
import java.util.concurrent.ConcurrentLinkedQueue;
import java.util.concurrent.TimeUnit;

@Service
public class ExportService {

    private final ObjectMapper objectMapper = new ObjectMapper();
    private final OmgevingswetConfig config;

    private final ConcurrentLinkedQueue<String> exportQueue = new ConcurrentLinkedQueue<>();
    private final ConcurrentHashMap<String, String> inputMap = new ConcurrentHashMap<>();
    private final ConcurrentHashMap<String, byte[]> outputMap = new ConcurrentHashMap<>();

    public String startExport(final String exportData) {
        final String exportKey = UUID.randomUUID().toString();
        inputMap.put(exportKey, exportData);
        exportQueue.add(exportKey);
        return exportKey;
    }

    public ExportStatus getStatus(final String exportKey) {
        if (outputMap.containsKey(exportKey)) {
            return ExportStatus.DONE;
        } else return ExportStatus.WAITING;
    }

    public byte[] getResult(final String exportKey) {
        final byte[] result = outputMap.get(exportKey);
        outputMap.remove(exportKey);
        return result;
    }

    public ExportService(final OmgevingswetConfig config) {
        this.config = config;

        final Thread thread = new Thread(this::runProcess);
        thread.start();
    }

    private void runProcess() {

        while (true) {
            final String exportKey = exportQueue.poll();
            File outputFile = null;
            try {
                if (exportKey == null) {
                    Thread.sleep(100);
                } else {
                    System.out.println("Exporting '" + exportKey + "'");

                    final String url = config.getRenderingUrl().replace("[KEY]", exportKey);

                    outputFile = File.createTempFile("VNG", ".pdf");

                    final Process process = Runtime.getRuntime().exec(List.of(
                            config.getChromeHeadlessExecutableName(),
                            "--headless",
                            "--virtual-time-budget=60000",
                            "--run-all-compositor-stages-before-draw",
                            "--no-sandbox",
                            "--print-to-pdf=" + outputFile.getAbsolutePath(),
                            "--print-to-pdf-no-header",
                            "--hide-scrollbars",
                            "--disable-gpu",
                            url
                    ).toArray(new String[0]));

                    process.waitFor(30, TimeUnit.SECONDS);

                    byte[] pdfBytes = Files.readAllBytes(outputFile.toPath());
                    outputFile.delete();

                    pdfBytes = writeMetadata(pdfBytes, inputMap.get(exportKey));

                    System.out.println("Done exporting '" + exportKey + "'");
                    outputMap.put(exportKey, pdfBytes);
                    inputMap.remove(exportKey);
                }
            } catch (IOException | InterruptedException e) {
                e.printStackTrace();
                if (outputFile != null && outputFile.exists()) {
                    outputFile.delete();
                }
            }
        }
    }

    private byte[] writeMetadata(final byte[] input, final String exportData) throws IOException {
        try (final PDDocument document = PDDocument.load(input);
             final ByteArrayOutputStream baos = new ByteArrayOutputStream()) {
            final PDDocumentInformation info = document.getDocumentInformation();
            info.setCustomMetadataValue(config.getMetaDataKey(), exportData);
            document.save(baos);
            return baos.toByteArray();
        }
    }

    public String getMetadata(final String exportKey) {
        return inputMap.get(exportKey);
    }
}
