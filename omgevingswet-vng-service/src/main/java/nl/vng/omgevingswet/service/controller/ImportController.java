package nl.vng.omgevingswet.service.controller;

import nl.vng.omgevingswet.service.services.ImportService;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.multipart.MultipartFile;

import java.io.IOException;

@RestController
public class ImportController {

    private final ImportService importService;

    public ImportController(final ImportService importService) {
        this.importService = importService;
    }

    @PostMapping("/import")
    public String import_(@RequestParam("file") MultipartFile file) throws IOException {
        return importService.getExportData(file.getBytes());
    }

}
