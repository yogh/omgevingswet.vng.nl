package nl.vng.omgevingswet.service.services;

import nl.vng.omgevingswet.service.config.OmgevingswetConfig;
import nl.vng.omgevingswet.service.domain.ExportStatus;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.Timeout;

import java.io.File;
import java.io.IOException;
import java.nio.file.Files;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertNotEquals;

class ExportServiceImportServiceTest {

    @Test
    @Timeout(30)
    void testExportAndImport() throws InterruptedException, IOException {
        final OmgevingswetConfig config = new OmgevingswetConfig();
        final ExportService exportService = new ExportService(config);
        final ImportService importService = new ImportService(config);

        final String exportData = "testDataToIncludeIntoThePdf";

        final String key = exportService.startExport(exportData);
        assertEquals(ExportStatus.WAITING, exportService.getStatus(key));

        while (exportService.getStatus(key) != ExportStatus.DONE) {
            Thread.sleep(100);
        }

        final byte[] result = exportService.getResult(key);
        if (false) {
            final File output = new File("test.pdf");
            Files.write(output.toPath(), result);
        }

        assertNotEquals(0, result.length);

        final String parsedExportData = importService.getExportData(result);
        assertEquals(exportData, parsedExportData);
    }

}