alerts:
- rule: DEPLOYMENT_FAILED
- rule: DOMAIN_FAILED
name: app
region: ams
services:
- dockerfile_path: omgevingswet-vng-service/Dockerfile
  gitlab:
    branch: main
    deploy_on_push: true
    repo: yogh/omgevingswet.vng.nl
  http_port: 8080
  instance_count: 1
  instance_size_slug: basic-xxs
  name: api
  routes:
  - path: /api
  source_dir: /omgevingswet-vng-service
static_sites:
- dockerfile_path: Dockerfile
  gitlab:
    branch: main
    deploy_on_push: true
    repo: yogh/omgevingswet.vng.nl
  name: client
  output_dir: /web
  routes:
  - path: /
  source_dir: /

